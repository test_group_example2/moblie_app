import 'package:flutter/material.dart';
void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() =>  _MyStatefulWidgetState();
}
String englishGreeting = "Hello flutter";
String spanishGreeting = "Hola flutter";
String ChineseGreeting = "你好,flutter";
String BangGreeting = "จิ้มน้ำจุ่มละก็จุ่มน้ำจิ้ม,flutter";

class _MyStatefulWidgetState extends State<HelloFlutterApp> {
  String displayText =  englishGreeting;

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting?
                    spanishGreeting : englishGreeting;
                  });
                },
                icon: Icon(Icons.refresh)
            ),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting?
                    ChineseGreeting : englishGreeting;
                  });
                },
                icon: Icon(Icons.eco)
            ),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting?
                    BangGreeting : englishGreeting;
                  });
                },
                icon: Icon(Icons.person)
            )

          ],
        ) ,
        body: Center(
          child: Text(
            displayText,
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
    );
  }
}